# DevOps agent AD application & SPN 
Follow [this for Terraform setting](https://docs.microsoft.com/en-us/azure/developer/terraform/authenticate-to-azure?tabs=bash#create-a-service-principal) and [this for Gitlab setting](https://docs.gitlab.com/ee/integration/azure.html)

## Step 1. Create a service principal and assign roles
Add these roles;
- `Contributor` role to the subscription to create Azure resources 
- [Optional] `User Access Administrator` role to the subscription to create AAD application and SPN
```bash
# This will create an AAD app and SPN with "Contributor" role
az ad sp create-for-rbac --name ${ServicePrincipalName} --role Contributor --scopes /subscriptions/${SubscriptionId}
# This will add "User Access Administrator" role & update secret
az ad sp create-for-rbac --name ${ServicePrincipalName} --role "User Access Administrator" --scopes /subscriptions/${SubscriptionId}
```

## Step 2. Add Web authentication platform with redirect URL 
- Go to Azure portal AAD > newly create app > Authentication > Add a platform
- Select "Web" and put the redirect URL `https://gitlab.example.com/users/auth/azure_activedirectory_v2/callback` for Gitlab 

## Step 3. Expose an API
- Go to Azure portal AAD > newly create app > Expose an API 
- Click Application ID URI Set

## Step 4. Add API permissions
Add these permissions;
- [Delegated] `User.Read.All` permission or `email`, `openid`, `profile` permissions to act as Gitlab authentication provider
- [Application] `Application.ReadWrite.All` permission to create AD application and SPN
- [Application] `Directory.ReadWrite.All` permission to create AD users and groups

The last two roles require admin permission. If that's not possible, just remove SPN and AAD group create script from Terraform and create them manually assuming the user has the permission to create them directly.
