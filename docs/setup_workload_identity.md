# Set up Workload identity (Manually via CLI)
## Components
- AKS cluster
  - OIDC enabled (Preview feature)
  - Mutating admission webhook installed (via Helm)
  - Service account
- Azure AD application (SPN)
  - Establish a trust relatonship by adding a federated credential
  - Role assignments to access desired azure resources (e.g. KV, ASB)
## 1. Get OIDC URL (After OIDC enabled)
```bash
# Get OIDC issuer
az aks show --resource-group ${ResourceGroupName} --name ${ClusterName} --query "oidcIssuerProfile.issuerUrl" -otsv
```

## 2. Deploy the Azure AD Workload Identity helm chart to the cluster
```bash
# First, create a mutating admission webhook
helm repo add azure-workload-identity https://azure.github.io/azure-workload-identity/charts
helm repo update
helm install workload-identity-webhook azure-workload-identity/workload-identity-webhook \
   --namespace azure-workload-identity-system \
   --create-namespace \
   --set azureTenantID="${AzureTenantID}"
```

## 3. Create a Federated Azure AD Application + a Service Principal
This step is done from Terraform script.

## 4. Create a kubernetes service account manifest with some azwi specific metadata
```bash
export SERVICE_ACCOUNT_NAMESPACE=default
export SERVICE_ACCOUNT_NAME=workload-identity-sa
```
Sample service account to apply via `kubectl apply -f serviceaccount.yaml`
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  annotations:
    azure.workload.identity/client-id: ${APPLICATION_CLIENT_ID}
  labels:
    azure.workload.identity/use: "true"
  name: ${SERVICE_ACCOUNT_NAME}
  namespace: ${SERVICE_ACCOUNT_NAMESPACE}
```

## 5. Configure our pods to run with the service account
Sample pod YAML to apply via `kubectl apply -f application.yaml`
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: quick-start
  namespace: ${SERVICE_ACCOUNT_NAMESPACE}
spec:
  serviceAccountName: ${SERVICE_ACCOUNT_NAME}
  containers:
    - image: ghcr.io/azure/azure-workload-identity/msal-go
      name: oidc
      env:
      - name: KEYVAULT_NAME
        value: ${KEYVAULT_NAME}
      - name: SECRET_NAME
        value: ${KEYVAULT_SECRET_NAME}
  nodeSelector:
    kubernetes.io/os: linux
```
