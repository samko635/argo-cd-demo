# Getting started
This is my sandbox template repo that's been built using Gitlab, Azure, Terraform, ArgoCD, Helm.

## Prerequisites
- Git repo (Gitlab, Github and etc.)
- [AD Application & SPN for Gitlab devops agent](docs/create_aad_spn.md)
- Terraform state remote storage (e.g. Azure storage account)
- Kubectl and AZ CLI installed in the local machine

## Gitlab CI
### CI requirements
- Docker image with az cli & terraform cli for infrastructure deploy (e.g. zenika/terraform-azure-cli)
- Set CI/CD environment variables to integrate with Azure (ARM_SUBSCRIPTION_ID, ARM_TENANT_ID, ARM_CLIENT_ID, ARM_CLIENT_SECRET, ARM_ACCESS_KEY)

## Kubectl CLI (Local setting)
```bash
# Run this to add AKS cluster to kubectl config
az aks get-credentials --resource-group ${ResourceGroupName} --name ${ClusterName} --admin

# Switch context if it's already been added
kubectl config view
kubectl config use-context ${ContextName}
```

## Argo CD (Local setting)
Installing Argo CD to the AKS cluster will be done via Gitlab CI using [this script](0-prerequisites/scripts/install_argocd.sh) as part of the CI pipeline.
Follow [this ArgoCD official guide](https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd) to install Argo CD on your K8s cluster.

### 1. Access ArgoCD dashboard
Once installed, argocd-server type needs to be changed to "LoadBalancer" for external access to the dashboard. This is being done by Terraform in this repo but if ArgoCD is deployed manually using kubectl and kustomize YAML, run the command below;
```bash
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

Gain the default admin secret (MUST change afterwards)
```bash
# Get the service IP (Check EXTERNAL-IP for argocd-server)
kubectl get svc -n argocd
# Get login secret (user name is 'admin' by default)
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
```

You can use terminal to access Argo CD directly. 
```bash
# Login
argocd login ${ARGOCD_SERVER}
# Add repo
argocd repo add https://...
```
### 2. Create Argo CD application
Once an application is created and connected to the repo, ArgoCD will automatically deploy the changes so that the application status syncs with the manifest files in the repo. This is an one-off activity.
```bash
# Run this once to synchronize Argo CD agent with our git repo
kubectl apply -n argocd -f application.yaml
```
When creating an application on ArgoCD, if the git repo is private, you must provide credentials accordingly. There are a few options for authentication but they are all associated with a git user credentials so a service user should be created for this purpose. (See [this instruction](https://argo-cd.readthedocs.io/en/stable/user-guide/private-repositories/))
- SSH private key (id_rsa)
- HTTPS using username/password
- PAT

## Install KEDA
This step is automated with Terraform script using helm release resource. You could simply enable KEDA add-on feature but it [only allows 2.7.0 version](https://docs.microsoft.com/en-us/azure/aks/keda-deploy-add-on-cli) as of Sept 20th, 2022. (No support for Azure Workload identity)
Follow [this guide](https://keda.sh/docs/2.8/deploy/) for installing KEDA in Kubernetes cluster, or [this guide](https://github.com/kedacore/sample-dotnet-worker-servicebus-queue/blob/main/workload-identity.md) for installing KEDA WITH Workload identity.

## Workload identity
This step is automated with Terraform script using helm release resource.
- [Quick & Easy official tutorial with CLI](https://azure.github.io/azure-workload-identity/docs/quick-start.html). Detail instructions for this step is documented [HERE](docs/setup_workload_identity.md)
- [Terraform Tutorial 1](https://dev.to/maxx_don/implement-azure-ad-workload-identity-on-aks-with-terraform-3oho)
- [Terraform Tutorial 2](https://github.com/Azure-Samples/azure-workload-identity-nodejs-aks-terraform)


## [Sealed secret](https://github.com/bitnami-labs/sealed-secrets)
### Secret controller
In order to use sealed secret, the Kubernetes cluster must have [secret controller](https://bitnami-labs.github.io/sealed-secrets#controller) installed. This can be installed via Kustomize file using release YAML from [here](https://github.com/bitnami-labs/sealed-secrets/releases) or via Helm.
```bash
# Install using kustomize
kubectl apply -f controller.yaml -n kube-system
```

### Kubeseal
Another prerequisites for generating sealed secret is the Kubeseal tool. Run this on the cluster VM.
```bash
# Linux install (e.g. version 0.17.3)
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/kubeseal-0.17.3-linux-amd64.tar.gz
tar -xvzf kubeseal-0.17.3-linux-amd64.tar.gz kubeseal
sudo install -m 755 kubeseal /usr/local/bin/kubeseal
```
And then export the cluster certificate with this commmand. This certificate can be used to encrypt/decrypt sealed secrets.
```bash
kubeseal --fetch-cert > my_kube_cert.pem
```

---
# KEDA demo (e.g. Service bus trigger)
This is used in `3-overlays-with-keda` application.

![image](docs/demo3_diagram.png)

### Option 1. Authenticate via [workload identity](https://github.com/kedacore/sample-dotnet-worker-servicebus-queue/blob/main/workload-identity.md) (RECOMMENDED)
Update the client id on `Service Account` with the newly created SPN app ID.
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  annotations:
    azure.workload.identity/client-id: "UPDATE_ME"
  labels:
    azure.workload.identity/use: "true"
  name: "workload-identity-sa"
  namespace: "workload-identity-demo-dev"
```
Newly created SPN would have `Azure Service Bus Data Receiver` role assigned to it so KEDA operator should be able to monitor the queue without any secret. 

### Option 2. Authenticate via [conn str](https://github.com/kedacore/sample-dotnet-worker-servicebus-queue/blob/main/connection-string-scenario.md)
```bash
# Create a manage rule for KEDA
az servicebus queue authorization-rule create --resource-group samko-training --namespace-name ${ASBNamespace} --queue-name orders --name keda-monitor --rights Manage Send Listen

# Encrypt the connection string with base64 representation
echo -n "connection string" | base64
```
Add encrypted connection string as a `Secret`
```yaml
# Only necessary for connection string auth
apiVersion: v1
kind: Secret
metadata:
  name: secrets-order-management
  labels:
    app: order-processor
data:
  servicebus-order-management-connectionstring: <base64-encrypted-connection-string>
```
