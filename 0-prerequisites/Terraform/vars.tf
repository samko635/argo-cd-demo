variable "baseTags" {
  default = {
    Owner   = "samko.yoon@servian.com",
    Purpose = "training",
    Client  = "Coles"
  }
  description = "Servian base resource tags"
  type        = map(string)
}

variable "userName" {
  type        = string
  default     = "yourName"
  description = "username used for resource prefix"
}

variable "env" {
  type        = string
  default     = "dev"
  description = "Environment name"
}

variable "aksNamespace" {
  type        = string
  default     = "default"
  description = "AKS namespace to deploy service account"
}

variable "aksServiceAccountName" {
  type        = string
  default     = "workload-identity-sa"
  description = "AKS namespace to deploy service account"
}