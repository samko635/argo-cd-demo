terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.1.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "2.20.0"
    }
    # kubernetes = {
    #   source  = "hashicorp/kubernetes"
    #   version = "2.10.0"
    # }
    helm = {
      source  = "hashicorp/helm"
      version = "2.5.1"
    }
  }
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstate9058"
    container_name       = "tfstate"
    key                  = "terraform.tfstate"
  }
}

# ================ Resource group ================ #
resource "azurerm_resource_group" "baseRG" {
  name     = "${var.userName}Training"
  location = "Australia East"
  tags     = var.baseTags
  lifecycle {
    ignore_changes = [tags]
  }
}

# ================ Modules ================ #
module "aks" {
  source             = "./modules/aks"
  baseTags           = var.baseTags
  userName           = var.userName
  env                = var.env
  aksAdminGroupObjId = azuread_group.aks_admins.object_id
  location           = azurerm_resource_group.baseRG.location
  rgName             = azurerm_resource_group.baseRG.name
}

module "asb" {
  source             = "./modules/asb"
  baseTags           = var.baseTags
  userName           = var.userName
  env                = var.env
  location           = azurerm_resource_group.baseRG.location
  rgName             = azurerm_resource_group.baseRG.name
}

# resource "local_file" "kubeconfig" {
#   depends_on = [module.aks]
#   filename   = "kubeconfig"
#   content    = module.aks.kube_config_raw
# }
