# ================ Key Vault ================ #
resource "random_string" "suffix" {
  length  = 5
  special = false
  upper   = false
}

resource "azurerm_key_vault" "demo" {
  name                       = "demokeyvault-${random_string.suffix.result}"
  location                   = azurerm_resource_group.baseRG.location
  resource_group_name        = azurerm_resource_group.baseRG.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  sku_name                   = "standard"
  soft_delete_retention_days = 7

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Create",
      "Get",
    ]

    secret_permissions = [
      "Set",
      "Get",
      "List",
      "Delete",
      "Purge",
      "Recover"
    ]
  }
}

# ================ Access policy ================ #
resource "azurerm_key_vault_access_policy" "aks_azwi_app" {
  key_vault_id = azurerm_key_vault.demo.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azuread_service_principal.directory_role_app.object_id

  secret_permissions = [
    "Get",
  ]
}

# ================ Secrets ================ #
resource "azurerm_key_vault_secret" "sample" {
  name         = "my-secret"
  value        = "hello"
  key_vault_id = azurerm_key_vault.demo.id
}

resource "azurerm_key_vault_secret" "aks_host" {
  name         = "aks-host"
  value        = module.aks.host
  key_vault_id = azurerm_key_vault.demo.id
}

resource "azurerm_key_vault_secret" "aks_client_certificate" {
  name         = "aks-client-certificate"
  value        = module.aks.client_certificate
  key_vault_id = azurerm_key_vault.demo.id
}

resource "azurerm_key_vault_secret" "aks_client_key" {
  name         = "aks-client-key"
  value        = module.aks.client_key
  key_vault_id = azurerm_key_vault.demo.id
}

resource "azurerm_key_vault_secret" "aks_cluster_ca_certificate" {
  name         = "aks-cluster-ca-certificate"
  value        = module.aks.cluster_ca_certificate
  key_vault_id = azurerm_key_vault.demo.id
}

resource "azurerm_key_vault_secret" "aks_kube_config_raw" {
  name         = "aks-kubeconfig"
  value        = module.aks.kube_config_raw
  key_vault_id = azurerm_key_vault.demo.id
}
