output "resource_group_name" {
  value = azurerm_resource_group.baseRG.name
}

output "aks_spn_client_id" {
  value = azuread_service_principal.directory_role_app.application_id
}

output "cluster_ca_certificate" {
  value = module.aks.cluster_ca_certificate
  sensitive = true
}

output "client_key" {
  value = module.aks.client_key
  sensitive = true
}
