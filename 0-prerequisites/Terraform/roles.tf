# Use this is cluster needs to pull an image from ACR
# resource "azurerm_role_assignment" "acr_image_puller" {
#   scope                = azurerm_container_registry.acr.id
#   role_definition_name = "AcrPull"
#   principal_id         = azurerm_kubernetes_cluster.k8s.kubelet_identity.0.object_id
# }

# data "azurerm_user_assigned_identity" "agentpool" {
#   name                = "agentpool"
#   resource_group_name = azurerm_resource_group.baseRG.id
#   depends_on = [
#     azurerm_kubernetes_cluster.aksCluster
#   ]
# }

# ================ Pod identity ================ #
resource "azurerm_role_assignment" "keda_asb_read" {
  scope                            = module.asb.servicebus_namespace_id
  role_definition_name             = "Azure Service Bus Data Receiver"
  principal_id                     = azuread_service_principal.directory_role_app.object_id
}

# resource "azurerm_role_assignment" "agentpool_msi" {
#   scope                            = azurerm_resource_group.baseRG.id
#   role_definition_name             = "Managed Identity Operator"
#   principal_id                     = data.azurerm_user_assigned_identity.agentpool.principal_id
#   skip_service_principal_aad_check = true
# }

# resource "azurerm_role_assignment" "agentpool_vm" {
#   scope                            = azurerm_resource_group.baseRG.id
#   role_definition_name             = "Virtual Machine Contributor"
#   principal_id                     = data.azurerm_user_assigned_identity.agentpool.principal_id
#   skip_service_principal_aad_check = true
# }


## Azure AD application that represents the app
# resource "azuread_application" "app" {
#   display_name = "sp-app-${var.env}"
# }

# resource "azuread_service_principal" "app" {
#   application_id = azuread_application.app.application_id
# }

# resource "azuread_service_principal_password" "app" {
#   service_principal_id = azuread_service_principal.app.id
# }

# ## Azure AD federated identity used to federate kubernetes with Azure AD
# resource "azuread_application_federated_identity_credential" "app" {
#   application_object_id = azuread_application.app.object_id
#   display_name          = "fed-identity-app-${var.env}"
#   description           = "The federated identity used to federate K8s with Azure AD with the app service running in k8s ${var.env}"
#   audiences             = ["api://AzureADTokenExchange"]
#   issuer                = var.oidc_k8s_issuer_url
#   subject               = "system:serviceaccount:${local.namespace_name}:${local.service_account_name}"
# }
