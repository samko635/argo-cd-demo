provider "azurerm" {
  features {}
}

provider "azuread" {
}

provider "helm" {
  kubernetes {
    host                   = module.aks.host
    client_certificate     = base64decode(module.aks.client_certificate)
    client_key             = base64decode(module.aks.client_key)
    cluster_ca_certificate = base64decode(module.aks.cluster_ca_certificate)
  }
}

# provider "kubernetes" {
#   host                   = module.aks.host
#   client_certificate     = base64decode(module.aks.client_certificate)
#   client_key             = base64decode(module.aks.client_key)
#   cluster_ca_certificate = base64decode(module.aks.cluster_ca_certificate)
#   # config_path = "./kubeconfig"
# }
