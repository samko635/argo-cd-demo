#!/bin/bash

# Enable preview features to the subscription
az extension add --upgrade --name aks-preview
az feature register --name AKS-KedaPreview --namespace Microsoft.ContainerService
az provider register --namespace Microsoft.ContainerService

# Update to enable KEDA (vers 2.7.0)
az aks update \
  --resource-group samkoTraining \
  --name samkoTrainingAKSCluster \
  --enable-keda
