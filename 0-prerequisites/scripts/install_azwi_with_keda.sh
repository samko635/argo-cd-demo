#!/bin/bash

# Helm repo update
helm repo add azure-workload-identity https://azure.github.io/azure-workload-identity/charts
helm repo add kedacore https://kedacore.github.io/charts
helm repo update

echo "INSTALL Azure Workload Identity"
helm install workload-identity-webhook azure-workload-identity/workload-identity-webhook \
   --namespace azure-workload-identity-system \
   --create-namespace \
   --set azureTenantID="${TENANT_ID}"

# INSTALL KEDA 2.8.0 with AZWI -----------
echo "INSTALL KEDA"
helm install keda kedacore/keda \
  --set podIdentity.azureWorkload.enabled=true \
  --set podIdentity.azureWorkload.clientId="${CLIENT_ID}" \
  --set podIdentity.azureWorkload.tenantId="${TENANT_ID}" \
  --namespace keda-system \
  --create-namespace
